Missing [systemd units](https://www.freedesktop.org/software/systemd/man/latest/systemd.unit.html) or [desktop entries](https://specifications.freedesktop.org/desktop-entry-spec/latest/index.html).
